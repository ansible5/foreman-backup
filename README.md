# Foreman & Ansible backup

This role will take a backup of the foreman database and specified ansible deployment files

## Requirements

Needs to run on the foreman machine

Needs SSH key authentication to be setup between foreman machine and storage server for transfer task to run if external storage is used
and 'storage_server' variable below needs to be set

## Role Variables

**Global variables: (defaults/main.yml)**

- foreman_db_file: this is where the foreman database password is read from
- backup_location: this is where the backup will be stored locally
- ansible_source_folder: this is the source folder for file backup, default: /opt/oiaas-deployment-ansible
- storage_server: remote storage server IP address/hostname only if external storage is used, not defined initially

**Local variables:**

- backup_db.yml

  login_host: the ip address of the server running the postgresql database, usually 127.0.0.1
  login_user: this is the user with access to the database

- backup_files.yml

additional files to be backed up are defined in the 'with_items' code block

## Dependencies

yum install postgresql-devel

pip install psycopg2

## Example Playbook

```ansible
---
- name: foreman backup
  hosts: localhost
  roles:
     - role: foreman-backup
       failure_is_critical: false
  tags:
    - backup

```

